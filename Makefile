# Build a summation C program

CC     = gcc
CFLAGS = -g -Wall

TARGET = summation

all: $(TARGET)

summation: summation.c
	$(CC) $(CFLAGS) -o $(TARGET) summation.c

clean:
	rm $(TARGET)

